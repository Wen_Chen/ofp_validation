# OFP Validation #

Compares the sequence identification between various identification methods provided a series of OTU tables and names files.  Designed to compare OFP output with other datasets as well.  

## Synopsis ##

	perl OFP_validate.pl -r [REFERENCE FILE]

## Example Reference File ##
```
#!text

[Header_1]
OTU_TABLE_PATH = < path to otu table 1>
NAMES_FILE_PATH = < path to names file coresponding with otu table 1 >
TYPE = < type of analysis > (i.e. QIIME, Mothur)
REGION = < gene region >
[Header_2]
OTU_TABLE_PATH = < path to otu table 2 >
NAMES_FILE_PATH = < path to names file corresponding with otu table 2 >
TYPE = < type of analysis >
REGION = < gene region >

```

## Options ##
Optional:


```
#!text
Compare OFP output:
-ofp        Flag to indicate OFP analysis
        -otu        [ otu table from OFP ]
        -fas        [ fasta file produced by OFP ]
-o  [ outpt directory ] <default is outdata>
-p  [ output prefix ]   <default is "validation">
-t  [ number of threads ]   <default is 1>
-g  [ taxa for summarizing identifications ]
        Accepted values:
            sk | superkingdom
            k | kingdom
            p | phylum
            c | class
            o | order
            f | family
            g | genus
            s | species
        -u  Flag to keep "Unassigned" matches in the summary output <default is to remove>
-s [ QIIME mapping file ]
```

## Output##

Tab file of sequence identifications for each method in the reference file and OFP if selected.

NA indicates a sequence was not found with a given identification method

If [ -g ] option is used 2 additional tab files are produced:

* One set for sequences with matching identification at the provided rank
* One set for sequences with identifications that do not match at the provided rank
    * Additional column in the mismatch file indicates the maximum number of matching identifications to assit with processing