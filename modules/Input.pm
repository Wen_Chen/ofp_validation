#!/usr/bin/env perl
package Input;
#license
#wet-boew.github.io/wet-boew/License-en.html / wet-boew.github.io/wet-boew/Licence-fr.html

use strict; use warnings;
use Data::Dumper;
use File::Basename;
use Getopt::Long qw(GetOptionsFromArray);

use lib dirname(__FILE__);
#use LogFile;
use Config::Simple;

##########################################################################################
#USAGE
#
##########################################################################################


sub _deepCopy {
  my $item = shift();
  if (ref $item eq "HASH") {
    my $hashref;
    while(my($key, $val) = each %$item) {
       $hashref->{$key} = _deepCopy($val);
    }
    return $hashref;
  } elsif (ref $item eq "ARRAY") {
    my $arrayref;
    foreach (@$item){
      push(@$arrayref, _deepCopy($_));
    }
    return $arrayref;
  } else {
    return $item;
  }
}

my $_settings         = {}; #the singleton that stores all settings for this run
my $_default_settings = {};
my $_input            = {};    #records command line input through the Getopt::Long module

#Functions///////////////////////////////////////////////////////////////////////////////////
sub Configure {
  return if %$_settings;
 
  $_default_settings = _deepCopy(shift());
  my $input_mapping  = shift();
  my $usage_fn       = shift();
  my $format_fn      = shift();  
  die("No help function given\n") unless defined($usage_fn);

  #SO UGLY :'(
  my $config_file = _readCMD($input_mapping, $usage_fn);
  _initFromConfig($config_file) if $config_file;
  _completeWithDefaults($format_fn, $input_mapping, $config_file); 

  return getSettings();
}

#process command line args, and see if a config file was specified for use
sub _readCMD {
  my ($input_mapping, $usage_fn) = @_;
  my $input_map = {};
  my @cmd_options;
 
  #print STDERR (Dumper($input_mapping)."\n");
 
  while(my($cmd, $data) = each %$input_mapping) {
    #print $data->{'config'}, "\n";
    die("This config option does not have a default\n") unless defined $_default_settings->{$data->{'config'}};
    $input_map->{(split(/\|/, $cmd))[0]}  = sub {$_settings->{$data->{'config'}} = $_input->{$cmd}};
    push(@cmd_options, $cmd.$data->{'type'});
  }

  GetOptionsFromArray(\@main::ARGV, $_input, @cmd_options, 'h|help', 'config=s') or die("$!"); #for a more exhaustive option list, create a config file.

  unless (defined $_input->{'h'} or defined $_input->{'config'} ) {
   	
	 &{$input_map->{$_}} foreach (keys %$_input);
    
  }

  if (defined $_input->{'h'}) {
    &$usage_fn;
    exit;
  }

  return $_input->{'config'};
}

sub _initFromDefault {
  $_settings = _deepCopy($_default_settings);
}

sub _initFromConfig {
  my $config_file = shift();  

  unless (-e $config_file) { #bad filename given
    die("[ERROR:] $config_file does not exist\n");
  } else { #read from a config file
    Config::Simple->import_from($config_file, $_settings);
  }
} 


sub _completeWithDefaults { 
  #if any options were not defined in the config file, this will fill in those options with the defaults.
  %$_settings = (%$_default_settings, %$_settings);

  my $special_formatting = shift();
  foreach (grep {$_->{'type'} =~ /@/} values shift()) {
    my $option = $_->{'config'};
    $_settings->{$option} = [$_settings->{$option}] unless (ref $_settings->{$option}); 
  }

  &$special_formatting($_settings, shift()) if defined($special_formatting);  
}

sub getSettings {
  return _deepCopy($_settings);
}

sub printErrSettings{
  print STDERR (shift()." _Settings::\n".Dumper($_settings)."\n");

}

1;
