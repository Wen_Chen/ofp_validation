#!/usr/bin/env perl
package LogFile;

use strict; use warnings;
use YAML;
use YAML::Dumper;

#use lib dirname(__FILE__);
#use dependencies::YAML::Tiny;

my $_logfile;
my @logSections = ('Date','Options','IO','Runtime');

sub addToLog {
  my $section  = shift();
  my $header   = shift();
  my $log_data = shift();

  my $valid_section = 0;
  foreach (@logSections){
    $valid_section = 1 if($section eq $_);
  }
  die("[ERROR:] Bad logfile section\n") unless $valid_section;

  $_logfile->{$section}->{$header} = $log_data;
}

sub writeLog { #assumes all sections are filled in
  my $log_path = shift();
  my $yaml = YAML::Dumper->new();

  open(my $lf, '>', $log_path);
  foreach my $section (@logSections) {
    #$yaml->[$i] = $_logfile->{$logSections[$i]};
    print $lf ($yaml->dump($_logfile->{$section}));
  }
  close($lf);
}

1;
