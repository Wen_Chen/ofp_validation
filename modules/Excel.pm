#!/usr/bin/env perl
package Excel;
use warnings;
use strict;
use Spreadsheet::ParseExcel;
use Spreadsheet::WriteExcel;


my $parser;
my $workbook;
my @worksheets;

sub init{
	#initialize excel parser since it reads from this file every time
	my $filename=shift;
	$parser = Spreadsheet::ParseExcel->new(); #initializes new sheet doc
	$workbook = $parser->parse($filename); #gets the doc name, evidently

	if (!defined $workbook){die;}
	#just need the first worksheet, so we use worksheet[0]
	#IDS at 4th y col
	#MID at 7th y col
	#info along the same xthrow. 
	@worksheets  = ($workbook->worksheets());

}

sub hyper_dimensional_excel_parser{
	#its hyper dimensional!

	my $filename = shift; #name of file that contains the sequence
	my $filehandle = shift;
	print $filehandle "\t454_SUMMARY_DATA:";

	#parses the filename
	#need the ID
	my @brokenname = split (/\./,$filename);
	my $cid = $brokenname[0];
	#need the mid#
	my $cmid = $brokenname[1];
	$cmid =~ s/MID//; #sub the mid
	$cmid =~ s/\_.*//; #sub the stuff after the _

	foreach (0 .. $worksheets[0]->row_range()){
		my $workid = $worksheets[0]->get_cell($_,3);
		my $midnum = $worksheets[0]->get_cell($_,6);
		if (defined $workid && ($workid->unformatted()) eq $cid && ($midnum->unformatted()) == $cmid){
			#we need every single element after a match (1-xx?)

			#print "much wow! it works! We found it!\n";
			#my $project = ($worksheets[0]->get_cell($_,2))->unformatted();
			#my $location= ($worksheets[0]->get_cell($_,14))->unformatted();			

			my $foundrow = $_;

			foreach (0 .. 49){

				my $key = ($worksheets[0]->get_cell(0,$_));
				my $val = ($worksheets[0]->get_cell($foundrow,$_));

				if (defined $key){
					print $filehandle ($key->unformatted(),":");	
				}
				if (defined $val){
					print $filehandle ($val->unformatted()),"\t";
				}
				#print $filehandle ";";
			} #get every column
			#print $filehandle "\n";
			return; 	
		}
	}

}

#get data for specific column based on id input
sub get_cell_by_id{
	my $filename = shift; #name of file that contains the sequence
	my $column =shift; 

	#parses the filename
	#need the ID
	my @brokenname = split (/\./,$filename);
	my $cid = $brokenname[0];
	#need the mid#
	my $cmid = $brokenname[1];
	$cmid =~ s/MID//; #sub the mid
	$cmid =~ s/\_.*//; #sub the stuff after the _

	foreach (0 .. $worksheets[0]->row_range()){
		my $workid = $worksheets[0]->get_cell($_,3);
		my $midnum = $worksheets[0]->get_cell($_,6);
		if (defined $workid && ($workid->unformatted()) eq $cid && ($midnum->unformatted()) == $cmid){
			my $project = ($worksheets[0]->get_cell($_,2))->unformatted();
			my $location= ($worksheets[0]->get_cell($_,14))->unformatted();
			my $foundrow = $_;
			if(defined $worksheets[0]->get_cell($foundrow,$column)){
				return $worksheets[0]->get_cell($foundrow,$column)->unformatted();
			}
		}
	}

}

#write to excel file
sub excel_writer{
	no warnings 'uninitialized'; #in case nothing was found
	#get filename and id list
	my $file=shift;
	my $ids = shift;
	my @ids = @{$ids};
	my $idhash=shift;
	my $filehash=shift;
	my $ofp_hash=shift;
	my $workbook = Spreadsheet::WriteExcel->new("$file.xls");
	my $worksheet = $workbook -> add_worksheet();
	my $column=1;
	
	#write labels
	$worksheet->write(0, 0, 'SeqID');
	$worksheet->write(0, 1,'OFP');
	foreach my $type(sort keys %$idhash){
		$column++;
		$worksheet->write(0,$column, $type);

	}
	#loop through all ids found and get the table
	for my $i (0 .. scalar(@ids)-1){
		$column=1;
		$worksheet->write($i+1,0, $ids[$i]);
		$worksheet->write($i+1,1,$ofp_hash->{$ids[$i]});
		#get all LCA and QIIME identifications
		foreach my $type (sort keys %$idhash){
			$column++;
			$worksheet->write($i+1,$column,$idhash->{$type}->{$ids[$i]});

		}
	}
	#write metadata labels
	my $metadata_sheet=$workbook->add_worksheet();
	$metadata_sheet->write(0,0,'SeqID');
	for my $i(0 .. 49){
		$metadata_sheet->write(0,$i+1,$worksheets[0]->get_cell(0,$i)->unformatted());

	}
	#write metadata
	for my $i(0 .. scalar(@ids)-1){
		$metadata_sheet->write($i+1,0,$ids[$i]);
		for my $j (0 .. 49){
			my $cur_file=$filehash->{$ids[$i]};
			#print "$cur_file\n";
			chomp $cur_file;
			$metadata_sheet->write($i+1,$j+1,get_cell_by_id($cur_file,$j));
		}
        }
}

1
