#!/usr/bin/perl env
use warnings;
use strict;
use List::MoreUtils qw/ uniq /;
use List::Util qw/ max /;
use File::Basename;
use FindBin;
use Cwd;
use threads;
use Thread::Queue;
use threads::shared;
use lib "$FindBin::Bin/modules";
use Input;
use LogFile;
use Bio::SeqIO;
use Spreadsheet::WriteExcel;
$| =1; 


#####################################################################################################
##############################################  Help  ###############################################
#####################################################################################################
sub help{
    print "OFP validate summarizes sequence identifications based on OTU tables and names files\n";
	print "Usage:\n";
	print "\tperl OFP_validate.pl -r [ reference file ]\n";
    print "Mandatory options:\n";
    print "\t-r\t\tReference file of datasets to compare\n\tPlease see manual or example reference file for formatting\n";
    print "Optional:\n";
    print "\tCompare OFP output:\n";
    print "\t\t-ofp\t\tFlag to indicate OFP analysis\n";
    print "\t\t-otu\t\t[ otu table from OFP ]\n";
    print "\t\t-fas\t\t[ fasta file produced by OFP ]\n\n";
    print "\t-o\t[ output directory ]\t<default is outdata>\n";
    print "\t-p\t[ output prefix ]\t<default is \"validation\">\n";
    print "\t-t\t[ number of threads ]\t<default is 1>\n";
    print "\t-g\t[ taxa for summarizing identifications ]\n";
    print "\t\tAccepted values:\n\t\t\tsk | superkingdom\n";
    print "\t\t\tk | kingdom\n";
    print "\t\t\tp | phylum\n";
    print "\t\t\tc | class\n";
    print "\t\t\to | order\n";
    print "\t\t\tf | family\n";
    print "\t\t\tg | genus\n";
    print "\t\t\ts | species\n";
    print "\t-u\tFlag to keep \"Unassigned\" matches in the summary output\t<default is to remove>\n";
    print "\t-s [ QIIME mapping file ]  << Warning: Assumes sequence ID format is Sample_SeqID >>\n";
	print "Use --config to specify a config file in .ini format. The configuration options will override all command line options.\n << Warning: Config file not not tested as of this release>>\n";
    print "\nOutput:\n";
    print "\tTab file of sequence identifications for each method in the reference file and OFP if selected.\n\tNA indicates a sequence was not found with a given identification method\n";
    print "\tIf [ -g ] option is used 2 additional tab files are produced:\n\t\tOne set for sequences with matching identification at the provided rank\n\t\tOne set for sequences with identifications that do not match at the provided rank\n";
}

#####################################################################################################
##########################################  Configuration  ##########################################
#####################################################################################################
my $default_settings = { #This is all config options, harcoded with defaults. 
                         #Could also use an external default file, but that's less secure
                        'OFP.flag'                               =>"0",                                     #  flag for if to check OFP output. default is no
                        'OFP.otu'                                => "",                                     #  path to OFP otu table, mandatory if OFP flag.  default is empty
                        'OFP.tab'                                =>"",                                      #  path to OFP fasta output, mandatory if OFP flag.  default is empty
                        'IO.output_dir'                          => "$FindBin::Bin/outdata",                #  default output directory
                        'SETTINGS.threads'                       => "1",                                    #  default number of threads
                        'REFERENCE.454_summary_table'            => "",                                     #  default path to 454 summary spreadsheet.  not mandatory
                        'REFERENCE.reference_folder'             => "",                                     #   path to file with references to check.  default empty.  mandatory
                        'IO.prefix'                              => "validation",                           #   default prefix for output
                        'SUMMARY.taxa'                           => "",                                     #  if summary is requested which taxon level to summerize at
                        'SUMMARY.subset'                         =>"0"                                     #   flag for keeping unassigned matches from summary default is to remove
                       };

my $input_mapping = {
                    'ofp'       => {'type' => '' , 'config' => 'OFP.flag'},
                    'otu'       => { 'type' => '=s', 'config' => 'OFP.otu'},
                    'fas'       => {'type' => '=s', 'config'  => 'OFP.tab'},
                    'o'         => {'type' => '=s' , 'config' => 'IO.output_dir'},
                    's'         => {'type' => '=s' , 'config' => 'REFERENCE.454_summary_table'},
                    'r'         => {'type' => '=s' , 'config' => 'REFERENCE.reference_folder'},
                    't'         => {'type' => '=i' , 'config' => 'SETTINGS.threads'},
                    'p'         => { 'type' => '=s' , 'config' => 'IO.prefix'},
                    'g'         => { 'type' => '=s' , 'config' => 'SUMMARY.taxa'},
                    'u'         => { 'type' => '', 'config' => 'SUMMARY.subset'}
                    };

######################################################################################################
############################################  Read Input  ############################################
######################################################################################################
my $settings = Input::Configure($default_settings,$input_mapping, \&help);


my $ofp_flag = $settings -> {'OFP.flag'};                       #OFP flag
my $ofp_fasta_name = $settings -> {'OFP.tab'};                  #OFP lca.tab file
my $ofp_otu_name = $settings -> {'OFP.otu'};                    #OFP otu table
my $output_dir= $settings ->{'IO.output_dir'};                  #the folder of the output
my $reference = $settings -> {'REFERENCE.reference_folder'};    #file with paths to things to compare
my $threads = $settings -> {'SETTINGS.threads'};                #threads
my $out_prefix = $settings -> {'IO.prefix'};                    #prefix for output
my $meta_data = $settings -> {'REFERENCE.454_summary_table'};   #meta data
my $summary_taxa = $settings -> {'SUMMARY.taxa'};               #rank to summarize by
my $summary_reduce = $settings -> {'SUMMARY.subset'};           #keep unassigned from matches

#####################################################################################################
#########################################  Global variables  ########################################
#####################################################################################################
#constant hash for taxon ranks
my $ACCEPTED_TAXONOMY = {
    'sk'    => "superkingdom",
    'k'     => "kingdom",
    'p'     => "phylum",
    'c'     => "class",
    'o'     => "order",
    'f'     => "family",
    'g'     => "genus",
    's'     => "species"
};

#get time stamp
my $DATE=POSIX::strftime('%Y_%B%d_%H_%M_%S',localtime());
my $HR_DATE=POSIX::strftime('%B %d, %Y %H:%M:%S',localtime());

#global hashes and variables
my $main_hash;                   #holds values from the reference file
my $reference_string;       #holds string representation from reference file
my %idhash : shared;        #holds the sequence id and the identificaiton found by various methods
my $idhash;                 #holds the sequence id and the identification found by various methods
my $start_time;             #start time
my @database_names;         #holds the information about databases for the logfile
my $out_tab;                #tabular output file
my $out_excel;              #excel output file
my $out_yaml;               #yaml output file
my $out_summary;             #summary file extensions
my $meta_hash;
my @meta_headers;

#Print progress variables used for get_classification
my $current : shared;
my $total;

#####################################################################################################
###########################################  Application  ###########################################
#####################################################################################################
&validate();
&initialize();


#add to logfile
LogFile::addToLog('Date','Date and time',$HR_DATE);
LogFile::addToLog('IO', 'Validate OFP output', $ofp_flag);
LogFile::addToLog('IO','OFP otu table',$ofp_otu_name);
LogFile::addToLog('IO','OFP fasta file', $ofp_fasta_name);
LogFile::addToLog('IO','Output folder',$output_dir);
LogFile::addToLog('IO','output prefix', $out_prefix);
LogFile::addToLog('Runtime','Summary taxa', $summary_taxa);
LogFile::addToLog('IO','Meta data table', $meta_data);
LogFile::addToLog('IO','Keep Unassigned matches in summary',$summary_reduce);
LogFile::addToLog('IO','Threads',$threads);
LogFile::addToLog('IO','Reference file',$reference);

#get full command line string
my $cmd_str= qx/ps -o args $$/;
$cmd_str=~s/COMMAND\n//g;
chomp $cmd_str;
LogFile::addToLog("Runtime","Command Line",$cmd_str);




print "\n======================================OFP VALIDATION============================================\n";
print "Using $threads threads\n";
print "Output directory $output_dir\n";
print "Output prefix $out_prefix\n";
if($ofp_flag){
    print "Comparing OFP results:\n\tFasta:\t$ofp_fasta_name\n\tOTU:\t$ofp_otu_name\n";
}
print "Using list of datasets to compare from $reference\n";
print "$reference_string";
if($meta_data){
    print "Using meta data from $meta_data\n";
}

if($ofp_flag){         
    #parse OFP output
    #information for log file
    push(@database_names,"OFP");
    print "\nProcessing OFP input\n";
    &parse_ofp_input($ofp_fasta_name, $ofp_otu_name);
    &get_class_ofp_reduced();                           
}else{
    #amalgamate OTU identifications for all provided references
    foreach my $type (sort keys %{$main_hash}){
        foreach my $key (sort keys %{$main_hash->{$type}}){
            my $full_name=join("_",$type,$key);
            print("Processing: $full_name\n");
            #information for logfile
            push(@database_names, $full_name);
            &get_classification($main_hash->{$type}->{$key}, $full_name);
        }
    }
}
&update_records($idhash);
print(scalar(keys($idhash))." sequences examined.\n");
if($summary_taxa){
print("Summarizing identifications at the ".$summary_taxa." rank\n");
    &summarize();
}
print("Printing output files\n");

&output_tabular($idhash, $out_tab);

#	#output for end of script
	my $end_time=time();
	my $total_time=$end_time-$start_time;
	#add to log
	LogFile::addToLog('Runtime','All comparisons', \@database_names);
	LogFile::addToLog('Runtime','Running time',"$total_time seconds");
	LogFile::writeLog("$out_yaml");
	
	print "============================================END=================================================\n";
	print "Process took $total_time seconds. \n";



#####################################################################################################
############################################  Functions  ############################################
#####################################################################################################

sub validate{
	#this function error checks validity of input files
    if($ofp_flag){
        unless (-e $ofp_fasta_name) {
            die "ERROR: OFP comparison selected and OFP input fasta file not found\n";
        }
        unless (-e $ofp_otu_name){
            die "ERROR: OFP comparision selected and OFP input OTU table not found\n";
        }
    }else{
        $ofp_fasta_name="";
        $ofp_otu_name="";
    }
    unless($reference && -e $reference){
        die "ERROR: No list of datasets to compare was provided\n";
    }
    #Validate pattern in reference file
    if($meta_data){
        if(!-e $meta_data){
            die "ERROR:  QIIME mapping file selected but does not exist\n";
        }
    }
    #Validate that the taxon ranks are accepted
    if($summary_taxa){
        $summary_taxa = lc($summary_taxa); #convert to lowercase for consistency
        if(!(grep(($_ eq $summary_taxa), keys($ACCEPTED_TAXONOMY)) or grep(($_ eq $summary_taxa), values($ACCEPTED_TAXONOMY)))){
            die "ERROR:  Taxon rank provided is not recognized.\n \tAccepted values are:\n\t\tsuperkingdom (sk)\n\t\tkingdom (k)\n\t\tphylum (p)\n\t\tclass (c)\n\t\torder (o)\n\t\tfamily (f)\n\t\tgenus (g)\n\t\tspecies (s)\n";
        }
    }
}


sub initialize{
    #initialize hashes and directories according to input
    $idhash= \%idhash;
    $main_hash={};
    $reference_string = "";
    $start_time = time();
    @database_names = ();
    #make output directory
    my $folder=join("_", $out_prefix,"validated.$DATE"); #timestamped folder
    if(! -d $output_dir ){
        mkdir "$output_dir";
    }
    if(-d "$output_dir/$folder"){
        die("ERROR:  output directory $output_dir/$folder already exists.  Please try again to update timestamp or select unique prefix\n");
    }else{
        mkdir "$output_dir/$folder";
    }
    
    #initialize datasets
    read_into_hash($reference);
    if($meta_data){
        $meta_hash={};
        &read_meta();
    }
    
    #outfiles
    $out_tab = "$output_dir/$folder/$out_prefix".".tab";
    $out_excel = "$output_dir/$folder/$out_prefix".".xls";
    $out_yaml = "$output_dir/$folder/$out_prefix".".yaml";
    
    if($summary_taxa){
        $out_summary = "$output_dir/$folder/$out_prefix"."_$summary_taxa"."_summary";
    }
}

#####Functions to read in hashes#####

sub read_into_hash{
    #read the contents in the reference file into a hash
    my $reference_file=shift;       #reference file
    open(FILE, "<$reference_file");
    my $type="";    #type of identification
    my $region;     #region targeted
    my $name;       #names file
    my $otu;        #otu table file
    foreach my $line (<FILE>){
        chomp $line;
        if($line =~m/\[/ and $line =~ m/\]/){           #if hit a header line and type, region, name, and otu are defined then add to hash
            if(defined $type and defined $region and defined $name and defined $otu){
                $main_hash->{$type}->{$region}->{"name"} = $name;
                $main_hash->{$type}->{$region}->{"otu"} = $otu;
                if(! -e $name || ! -e $otu ){
                    die "ERROR: Invalid path to $name or $otu"
                }
            $reference_string = $reference_string."$type \t $region \n\t Names file: $name\n\t OTU: $otu\n";
            }elsif($type ne ""){
                die "ERROR: Invalid formatting of reference file.\n";
            }
            undef $type;
            undef $region;
            undef $name;
            undef $otu;
        }
        else{  #see which line the file is on and set appropriate variable
            my @split = split (/=/,$line);
            if(defined $split[1]){
                $split[1]=~s/^\s+//; # removes leading white space
                $split[1]=~s/\s+$//; # removes trailing white space
            }
            
            if($line =~m/^OTU/){
                $otu=$split[1];
            }
            elsif($line =~/^NAME/)	{
                $name=$split[1];
            }
            elsif($line =~/^TYPE/){
                $type=$split[1];
            }
            elsif($line =~/^REGION/){
                $region=$split[1];
            }
        
        }
    }
    #set last entry in the file to hash
    if(defined $type and defined $region and defined $name and defined $otu){
        $main_hash->{$type}->{$region}->{"name"} = $name;
        $main_hash->{$type}->{$region}->{"otu"} = $otu;
        if(! -e $name || ! -e $otu ){
            die "ERROR: Invalid path to $name or $otu"
        }
        $reference_string = $reference_string."$type \t $region \n\t Names file: $name\n\t OTU: $otu\n";
    }
    else{
        die "ERROR:  Invalid formatting of reference file.\n";
    }
    close FILE;
}

#load metadata into a hash with sample as the key and meta data in a string as the value
sub read_meta{
    open(METAFILE, $meta_data);
    my @file_lines = <METAFILE>;
    close(METAFILE);
    my $h = "^#";
    my $s = "SampleID";
    foreach my $line (@file_lines){
        chomp($line);
        if($line =~m/$h$s/){ #search for header line which should start with #SampleID
            $line=~s/$h$s/$s/;
            @meta_headers = split(/\t/, $line);
        }elsif(!($line=~m/$h/)){ #any other lines without a comment contain data
            my @line_array = split(/\t/, $line);
            if($meta_hash->{$line_array[0]}){  #Make sure that the sample ids are unique
                die("ERROR:  SampleID's found in the Qiime mapping file are not unique.  $line_array[0] already found\n");
            }
            my $counter=0;
            foreach my $t (@line_array){
                $meta_hash->{$line_array[0]}->{$meta_headers[$counter]}=$line_array[$counter];
                $counter++;
            }
        }
    }
}

##########

#read OFP seqid's and identifications into the idhash
sub parse_ofp_input{
	my $fasta= shift; #filename of oligos
	my $otu = shift;
	my $id;
	my $lca_id;
	my $in = Bio::SeqIO->new(-file=>$fasta, -format=>"fasta");
	while (my $seq = $in->next_seq()){
		$id=$seq->id();
	        $lca_id=$seq->description();  #LCA identificaiton is found in description of fasta header
	        if(!$lca_id){
        		die "ERROR:  No LCA identification found in fasta header description.  Fasta file may not be from OFP output.  Please verify input.\n";
	        }
	$lca_id=(split(':',$lca_id))[1];
        $lca_id=(split("_OLIGO", $lca_id))[0];
        my $temp_command="$lca_id".'$';
	$temp_command =~ s/\|/\\\|/g;
        my $otuline=`grep $temp_command $otu`; #get line from OTU table with appropriate identification
        if(!$otuline){
            die "ERROR: Identification from fasta file does not exist in the OTU table provided.  Please verify input.\n";
        }
        my @otu_array = split("\t", $otuline);
        my $ofp_id = $otu_array[-2]; #get the identification from taxonomy column
        my %p : shared;
        my $p = \%p;
        $idhash->{$id}=$p;
        my %c : shared;
        my $c = \%c;
        $idhash->{$id}->{"OFP"}=$c;
        $idhash->{$id}->{"OFP"}=$ofp_id;  #Add to idhash
	}
}

#
##get classification from all databases
sub get_classification {
	my $input = shift;          #Hash
	my $full_name = shift;      #LCA_ITS1, QIIME_16SF, etc
    
	#get files
	my $id_map= $input->{"name"};                   #Names file
	my $otu_table = $input->{"otu"};                #Otu table
	
	#read file contents into array
	open (SFOTU,$id_map) or die "Cannot open $id_map";
	my @otu_id_map=<SFOTU>;  #load names file into an array
	close SFOTU or die $!;
    
	#print progress
	$total=scalar (@otu_id_map);
	$current=0;
	my $queue = Thread::Queue->new(@otu_id_map);
	my @threads_t;
	for(1 .. $threads){
        #looping through all ids to get an identification
        #update progress
        push @threads_t, threads ->new (
        sub{
            while(my $cur_id = $queue->dequeue_nb){
                $current++;
                print("Processed $current/$total OTUs\r");
                my @split_cur_id = split('\t', $cur_id);
                my $otu = shift(@split_cur_id);
                chomp($otu);
                my $searchOTU = "^"."$otu"."\\s";
                my $otu_id = `grep "$searchOTU" $otu_table`;  #get line from OTU table matching the OTU being processed
                if(!$otu_id){
                    die "ERROR: OTU number from names file does not exist in the OTU table provided.  Please verify input.\n";
                }
                $otu_id = (split("\t", $otu_id))[-1];
                while (my $seqid=shift(@split_cur_id)){
                    chomp($seqid);
                    chomp($otu_id);
                    unless (exists $idhash->{$seqid})
                    {
                        my %p : shared;  # temporary hash 
                        my $p = \%p;
                        $idhash->{$seqid} = $p;
                    }
                    my %c : shared; # temporary hash
                    my $c = \%c;
                    $idhash->{$seqid}->{$full_name} = $c;
                    $idhash->{$seqid}->{$full_name}=$otu_id;  #add to idhash
                }
            }
        });
        
	}
    $_->join for @threads_t;
    print("\n");
}

# get classifications for sequences based on OFP input
# should be faster than a regular comparison
sub get_class_ofp_reduced{
    foreach my $type (sort keys %{$main_hash}){
        foreach my $key (sort keys %{$main_hash->{$type}}){
            my $full_name=join("_",$type,$key);
            #information for logfile
            push(@database_names, $full_name);
            my $input_otu_map = $main_hash->{$type}->{$key}->{"name"};
            my $input_otu_table = $main_hash->{$type}->{$key}->{"otu"};
            print("Comparing OFP with $full_name\n");
            my @threads_t;
            my $queue = Thread::Queue->new(keys($idhash));
            for(1 .. $threads){
                push @threads_t, threads ->new(
                sub {
                    while(my $ofp_seqID = $queue->dequeue_nb){
                        my $input_otu_map_line = `grep $ofp_seqID $input_otu_map`; #using seqID from OFP get line from the input names file
                        if($input_otu_map_line){
                            my $input_otu = (split('\t', $input_otu_map_line))[0];
                            my $searchOTU = "^"."$input_otu"."\\s";
                            my $otu_id = `grep "$searchOTU" $input_otu_table`;
                            if(!$otu_id){
                                die "ERROR: OTU number from names file does not exist in the OTU table provided.  Please verify input.\n";
                            }
                            $otu_id = (split("\t", $otu_id))[-1];
                            chomp($otu_id);
                            my %c : shared; #temporary hash
                            my $c = \%c;
                            $idhash->{$ofp_seqID}->{$full_name} = $c;
                            $idhash->{$ofp_seqID}->{$full_name}=$otu_id;
                            
                        }else{  #Sequence ID from OFP not found in reference, set identification to "N/A"
                            my %c : shared; #temporary hash
                            my $c = \%c;
                            $idhash->{$ofp_seqID}->{$full_name} = $c;
                            $idhash->{$ofp_seqID}->{$full_name}="NA";
                        }
                    }
                });
            }
            $_->join for @threads_t;
        }
    }
}


#Create the summary of the validation based on a provided rank
sub summarize{
    #get short and full length representatives of taxa
    my $short_name;
    my $long_name;
    if(grep(($_ eq $summary_taxa), keys($ACCEPTED_TAXONOMY))){
        $short_name = $summary_taxa;
        $long_name = $ACCEPTED_TAXONOMY->{$summary_taxa};
    }else{
        $long_name = $summary_taxa;
        my $i = 0;
        ++$i until (values($ACCEPTED_TAXONOMY))[$i] eq $summary_taxa;
        $short_name = (keys($ACCEPTED_TAXONOMY))[$i];
    }
    my $split_pattern = "$short_name"."__";
    
    #Compare all sequences at defined rank
    my %matches : shared;
    my $matches = \%matches;
    my %mismatches : shared;
    my $mismatches = \%mismatches;
    my %num_misMatch : shared;
    my $num_misMatch = \%num_misMatch;
    my @threads_t;
    my $queue = Thread::Queue->new(sort(keys($idhash)));
    
    for(1 .. $threads){
        push @threads_t, threads ->new(
        sub{
            while (my $seqID = $queue->dequeue_nb){
                my $mismatch=0;             #assume mismatch=false i.e. that identifications match
                my $current_id = "";
                foreach my $key (sort keys $idhash->{$seqID}){
                    my $temp_value = $idhash->{$seqID}->{$key};
                    chomp $temp_value;
                    my $interest_id = (split($split_pattern, $temp_value))[1]; #get everything to the right of the taxon header of interest
                    if($interest_id){           #has some value
			$interest_id =~ s/\s/_/ ;
                        $interest_id = (split(/[;(\s\()]/, $interest_id))[0]; #split on semicolon at end of line
                        if(!$interest_id){ # if not defined then has no value and set to Not assigned
                            $interest_id="$split_pattern"."Not_assigned";
                        }
                        if(!$current_id){ #if current id is not set then set it to current id
                            $current_id = $interest_id;
                        }else{ #check if id matches
                            if(!($current_id eq $interest_id)){
                                $mismatch=1;
                                $num_misMatch->{$seqID} = &calculateMismatch($idhash->{$seqID}, $split_pattern);
                                last();
                            }
                        }
                    }else{
                        if(!$current_id){ #has the currentid been set if not then set it to Unassignable
                            $current_id=$temp_value;
                        }elsif(!($current_id eq $temp_value)){ #if current id is set and is not unassignable then set mismatch and break the loop
                            $mismatch = 1;
                            $num_misMatch->{$seqID} = &calculateMismatch($idhash->{$seqID}, $split_pattern);
                            last();
                        }
                    }
                }
                if($mismatch){
                    $mismatches->{$seqID} = $idhash->{$seqID};
                }else{
                    if(!($summary_reduce)){
                        if((values($idhash->{$seqID}))[0] ne "Unassignable"){
                            $matches->{$seqID} = $idhash->{$seqID};
                        }
                    }else{
                        $matches->{$seqID} = $idhash->{$seqID};
                    }
                }
            }
        });
    }
    $_->join for @threads_t;
    &output_tabular($matches,"$out_summary"."_matches.tab");
    &output_tabular($mismatches, "$out_summary"."_mismatches.tab", $num_misMatch);
}

# Calculate the maximum number of identifications for anything where there is a mismatch
# Return the maximum number of matching identifications
sub calculateMismatch{
    my $record = shift();
    my $pattern = shift();
    my @compare = values($record);
    my %test;
    foreach(@compare){
        my $temp_id = (split($pattern, $_))[1];
        if(!$temp_id){
            if($_ eq "NA"){
                $temp_id = "NA";
            }else{
                $temp_id="Unassigned";
            }
        }else{
             $temp_id = (split(/[;(\s\()]/, $temp_id))[0];
            if(!$temp_id){
                $temp_id ="$pattern"."Not_assigned";
            }
        }
        if(!$test{$temp_id}){
            $test{$temp_id}=0;
        }
        $test{$temp_id}++;
    }
    $test{"NA"} = 0;   #don't want unassigned or not present sequences to count toward total max matches
    $test{"Unassigned"} = 0;
    return (max(values(%test)));
}

# get the types of identifications from the idhash
sub get_id_type{
    my @id_type;
    foreach my $seqID (sort keys $idhash){
        foreach my $key (sort keys $idhash->{$seqID}){
            push(@id_type, $key);
            @id_type=sort(uniq(@id_type));
        }
    }
    return @id_type;
}

#   Update the hash to put "NA" for any sequences that are missing (condition where sequence was present with analysis but not another)
sub update_records{
    my $records = shift();
    my @id_type = sort(&get_id_type());
    
    my @threads_t;
    my $queue = Thread::Queue->new(sort(keys($idhash)));
    
    for(1 .. $threads){
        push @threads_t, threads ->new(
        sub{
            while (my $seqID = $queue->dequeue_nb){
                if(scalar(@id_type) ne scalar(keys($records->{$seqID}))){
                    foreach(@id_type){
                        if(!($records->{$seqID}->{$_})){
                            $records->{$seqID}->{$_} = "NA";
                        }
                    }
                }
            }
        });
    }
    $_->join for @threads_t;
}

#####################################################################################################
#############################################  Printing  ############################################
#####################################################################################################

#print the tabular output
sub output_tabular{
    my $out_hash = shift(); #hash to print
    my $out_f = shift();  #outfile
    my $match_count = shift(); #hash of match_counts
    my @id_type = sort(&get_id_type());
    open(OUT_TAB_HANDLE, ">$out_f") or die ("ERROR: Could not open output file $out_tab");
    print(OUT_TAB_HANDLE "SEQID");
    foreach(@id_type){
        print(OUT_TAB_HANDLE "\t$_");
    }
    if($match_count){ #if max matches is provided add this to the header
        print(OUT_TAB_HANDLE "\tMaxMatches");
    }
    if($meta_data){  # if the metadata was provided then add meta data fields to the headers
        foreach(@meta_headers){
            print(OUT_TAB_HANDLE "\t$_");
        }
    }
    print(OUT_TAB_HANDLE "\n");
    foreach my $seqID (sort keys $out_hash){
        print(OUT_TAB_HANDLE "$seqID"); # print the sequence ID
        foreach my $key (sort keys $out_hash->{$seqID}){
            my $t = $out_hash->{$seqID}->{$key};
            print(OUT_TAB_HANDLE "\t$t");       #print the identification
        }
        if($match_count){  #if max matches was profided print the max number of matches
            print(OUT_TAB_HANDLE "\t".$match_count->{$seqID});
        }
        if($meta_data){  #if meta data print meta data field if present
            foreach (@meta_headers){
                my $id = (split("_", $seqID))[0];
                if(!$id){
                    print("WARNING: sequence id is not in expected format Sample_SequenceID no metadata can be found\n");
                    $id=0;
                }
                my $t = $meta_hash->{$id}->{$_};
                if($t){
                    print(OUT_TAB_HANDLE "\t".$t);
                }else{
                    print(OUT_TAB_HANDLE "\tNA");
                }
            }
        }
        print(OUT_TAB_HANDLE "\n");
    }
    close(OUT_TAB_HANDLE);
}

#print the excel sheet
sub print_excel{
    my $out_hash = shift(); #hash to print
    my $out_f = shift();  #outfile
    my $match_count = shift();
    my @id_type = sort(&get_id_type());
    my $workbook = Spreadsheet::WriteExcel->new($out_f);
    my $worksheet = $workbook -> add_worksheet();
    my $format = $workbook->add_format();
    $format->set_bold();
    my $row = 0;
    my $col = 0;
    $worksheet->write($row, $col, "SeqID", $format);
    $col++;
    foreach(@id_type){
        $worksheet->write($row, $col, $_, $format);
        $col++;
    }
    if($match_count){
        $worksheet->write($row, $col, "MaxMatches", $format);
    }
    $col=0;
    foreach my $seqID (sort keys $out_hash){
        $row++;
        $worksheet->write($row, $col, $seqID);
        foreach my $key (sort keys $out_hash->{$seqID}){
            $col++;
            my $t = $out_hash->{$seqID}->{$key};
            $worksheet->write($row, $col, $t);
        }
        if($match_count){
            $col++;
            $worksheet->write($row, $col, $match_count->{$seqID});
        }
        if($meta_data){
            foreach (@meta_headers){
                $col++;
                my $id = (split("_", $seqID))[0];
                if(!$id){
                    print("WARNING: sequence id is not in expected format Sample_SequenceID no metadata can be found\n");
                    $id=0;
                }
                my $t = $meta_hash->{$id}->{$_};
                if($t){
                    $worksheet->write($row, $col, $t);
                }else{
                    $worksheet->write($row, $col, "NA");
                }
            }
        }
        $col=0;
    }
    $workbook->close();
}

#####################################################################################################
#############################################  Testing  #############################################
#####################################################################################################

sub printvalues{
    print "OFP flag \t $ofp_flag \n";                     #OFP flag
    print "OFP fasta file \t $ofp_fasta_name \n";                  #OFP lca.tab file
    print "OFP OTU file \t $ofp_otu_name \n";                    #OFP otu table
    print "Output directory \t $output_dir \n";                  #the folder of the output
    print "List of databases \t $reference \n";    #file with paths to things to compare
    print "Threads \t $threads \n";                #threads
    print "prefix \t $out_prefix \n";                    #prefix for output
    print "Summary taxa \t $summary_taxa";
}

sub printhash{
    my $ph = shift();
    foreach my $seqID (sort keys $ph){
        print("$seqID:\n");
        foreach my $key (sort keys $ph->{$seqID}){
            my $t = $ph->{$seqID}->{$key};
            print("\t $key \t $t\n");
        }
    }

}



